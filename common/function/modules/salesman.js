export default {
	/**
	 * 获取姓名+昵称
	vk.myfn.salesman.getName(userInfo);
	 */
	getName(userInfo = {}) {
		let vk = uni.vk;
		let nickname = userInfo.nickname;
		let real_name = vk.pubfn.getData(userInfo, "fx.real_name");
		let str = nickname
		if (real_name && real_name !== nickname) {
			str = `${real_name}（${nickname}）`;
		}
		return str;
	},
	/**
	 * 获取h5分享地址
	let rootPath = vk.myfn.salesman.getH5RootPath();
	 */
	getH5RootPath(mchInfo) {
		let vk = uni.vk;
		let rootPath = "";
		if (!mchInfo) mchInfo = vk.getVuex("$user.mchInfo") || {};
		let domain = vk.pubfn.getData(mchInfo, "h5.domain");
		let path = vk.pubfn.getData(mchInfo, "h5.path");
		if (domain) {
			if (path) {
				rootPath = `${domain}/${path}`;
			} else {
				rootPath = domain;
			}
		} else {
			// #ifdef H5
			rootPath = vk.h5.getRootPath();
			// #endif
		}
		return rootPath;
	},
	/**
	 * 获取默认分享设置
	let share = vk.myfn.salesman.getDefaultShare();
	 */
	getDefaultShare(mchInfo) {
		let vk = uni.vk;
		if (!mchInfo) mchInfo = vk.getVuex("$user.mchInfo") || {};
		let defaultShare = vk.pubfn.getData(mchInfo, "share",{});
		// #ifdef H5
		let env = vk.h5.getEnv();
		if (env === "h5-weixin") {
			return vk.pubfn.getData(mchInfo, "h5_weixin.share", defaultShare);
		} else if (env === "h5") {
			return vk.pubfn.getData(mchInfo, "h5.share", defaultShare);
		} else {
			return defaultShare;
		}
		// #endif
		// #ifdef MP-WEIXIN
		return vk.pubfn.getData(mchInfo, "mp_weixin.share", defaultShare);
		// #endif
		// #ifdef APP-PLUS
		return vk.pubfn.getData(mchInfo, "app.share", defaultShare);
		// #endif
	}
};
