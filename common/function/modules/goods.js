export default {
	/**
	 * 商品写入缓存
	 * vk.myfn.goods.set(goods);
	 */
	set(goods) {
		let vk = uni.vk;
		let goodsData = vk.getVuex('$goods.data');
		goodsData[goods._id] = goods;
		vk.setVuex('$goods.data', goodsData);
	},
	/**
	 * 商品批量写入缓存
	 * vk.myfn.goods.set(goods);
	 */
	setList(list) {
		let vk = uni.vk;
		let goodsData = vk.getVuex('$goods.data');
		list.map((goods, index) => {
			goodsData[goods._id] = goods;
		});
		vk.setVuex('$goods.data', goodsData);
	},
	/**
	 * 跳转到商品详情页
	 * vk.myfn.goods.pageToDetail(goods);
	 */
	pageToDetail(goods) {
		let vk = uni.vk;
		if (typeof goods === "object" && goods._id) {
			goods.view_count++;
			vk.myfn.goods.set(goods);
			vk.navigateTo({
				url: `/pages/goods/goods-detail?_id=${goods._id}`,
				success: function(res) {
					// 通过eventChannel向被打开页面传送数据
					res.eventChannel.emit('data', goods)
				}
			});
		} else {
			// 如果goods是1个字符串,则直接认为传的是_id
			vk.navigateTo(`/pages/goods/goods-detail?_id=${goods}`);
		}
	},
	/**
	 * 获取电子卡券商品的有效期文本
	 * vk.myfn.goods.getValidityTimeStr(goods);
	 */
	getValidityTimeStr(goods = {}) {
		let {
			type,
			exp_time_mode,
			exp_time_range,
			exp_time_rule
		} = goods;
		let str = "";
		if (type === 1) {
			if (exp_time_mode === 1) {
				str = `有效期: ${vk.pubfn.timeFormat(exp_time_range[0],'yyyy-MM-dd hh:mm')} - ${vk.pubfn.timeFormat(exp_time_range[1],'yyyy-MM-dd hh:mm')}`;
			} else if (exp_time_mode === 2) {
				let day = exp_time_rule[0] === 0 ? '当' : exp_time_rule[0];
				let end = exp_time_rule[1];
				str = `购买后${day}天生效，有效期${end}天`;
			} else {
				str = `购买后长期有效`;
			}
		}
		return str;
	}

};
