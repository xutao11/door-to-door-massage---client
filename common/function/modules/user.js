export default {
	/**
	 * 获取我成为vip的天数
	vk.myfn.user.getVipDay();
	 */
	getVipDay() {
		let time1 = vk.getVuex('$user.userInfo.vip_get_time');
		if (!time1) return 0;
		let time2 = Date.now();
		let { todayStart: todayStart1 } = vk.pubfn.getCommonTime(time1);
		let { todayStart: todayStart2 } = vk.pubfn.getCommonTime(time2);
		let time = todayStart2 - todayStart1;
		let day = time / (1000 * 3600 * 24) + 1;
		return Math.floor(day);
	},
	/**
	 * 自动更新vuex内的用户信息
	vk.myfn.user.getMyInfo();
	 */
	getMyInfo() {
		// 更新vuex内的用户信息
		vk.callFunction({
			url: 'client/user.getMyInfo'
		});
	},
	/**
	 * 是否是核销员
	 * vk.myfn.user.isVerify()
	 */
	isVerify() {
		return vk.getVuex('$user.userInfo.is_verify_staff');
	}
};
