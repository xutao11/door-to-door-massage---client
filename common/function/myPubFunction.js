/**
 * 自定义公共函数
 */

import cart from "./modules/cart";
import goods from "./modules/goods";
import salesman from "./modules/salesman";
import user from "./modules/user";

var myfn = {
	cart,
	goods,
	salesman,
	user,
};

/**
 * 带缓存的请求
vk.myfn.callFunctionForCache({
	url:"",
	loading:{ name:"loading", that },
	data:{

	},
	success: function(data) {

	}
});
 */
myfn.callFunctionForCache = function (obj) {
	let vk = uni.vk;
	let { url, data = {}, loading } = obj;
	// 定义为pub-缓存，可随时清理pub-缓存
	let keyName = `pub-${url}-${JSON.stringify(data)}`;
	// 读取本地缓存
	let cache = vk.getStorageSync(keyName);
	if (cache) {
		if (loading) loading.that[loading.name] = false;
		if (typeof obj.success === "function") obj.success(cache);
	} else {
		if (loading) loading.that[loading.name] = true;
	}
	// 不管是否有缓存，请求依然执行，同时更新缓存。
	vk.callFunction({
		url,
		data,
		success: function (data) {
			if (typeof obj.success === "function") obj.success(data);
			// 将数据缓存到本地，下次打开时，先加载本地数据，使页面可以快速展示！
			vk.setStorageSync(keyName, data);
		},
		fail: obj.fail,
		complete: function (data) {
			if (loading) loading.that[loading.name] = false;
			if (typeof obj.complete === "function") obj.complete(data);
		},
	});
};

/**
 * 小程序通用分享
onShareAppMessage(options) {
	return vk.myfn.onShareAppMessage({ options });
},
 */
myfn.onShareAppMessage = function (obj = {}) {
	let { options, path, title, imageUrl } = obj;
	let vk = uni.vk;
	let my_invite_code = vk.getVuex("$user.userInfo.my_invite_code");
	if (!path) {
		let { fullPath } = vk.pubfn.getCurrentPage();
		path = fullPath;
	}
	// 带上我的分享码
	if (my_invite_code) {
		path += path.indexOf("?") == -1 ? "?" : "&";
		path += `inviteCode=${my_invite_code}`;
	}
	// 获取商家设置的默认分享信息
	let defaultShare = vk.myfn.salesman.getDefaultShare();

	let share = {
		title: title || defaultShare.title,
		path,
		imageUrl: imageUrl || defaultShare.image,
	};
	return vk.pubfn.copyObject(share);
};

/**
 * APP通用分享
vk.myfn.appShare({
	provider: "weixin",
	scene: "WXSceneSession",
	type: 5, // 0:h5 5:小程序
	title,
	summary,
	imageUrl,
	pagePath,
});
 */
myfn.appShare = function (obj) {
	// #ifdef APP-PLUS
	let vk = uni.vk;
	let {
		provider = "weixin",
		scene = "WXSceneSession",
		type = 5, // 0:h5 5:小程序
		title,
		summary,
		imageUrl,
		pagePath,
	} = obj;
	let nickname = vk.getVuex("$user.userInfo.nickname");
	let avatar = vk.getVuex("$user.userInfo.avatar");
	let my_invite_code = vk.getVuex("$user.userInfo.my_invite_code");
	let rootPath = vk.myfn.salesman.getH5RootPath();
	let h5Link = rootPath + pagePath;
	let mchInfo = vk.getVuex("$user.mchInfo") || {};
	let share = vk.myfn.salesman.getDefaultShare() || {};
	if (!title) title = share.title;
	if (!imageUrl) imageUrl = share.image;
	if (!summary) summary = `来自 ${nickname} 的分享`;
	// 带上我的分享码
	if (my_invite_code) {
		pagePath += pagePath.indexOf("?") == -1 ? "?" : "&";
		pagePath += `inviteCode=${my_invite_code}`;
	}
	let { debug } = vk.getConfig();
	console.log("pagePath: ", pagePath);
	console.log("share: ", share);
	uni.share({
		provider,
		scene,
		type,
		href: h5Link,
		title,
		summary,
		imageUrl,
		miniProgram: {
			id: share.mp_weixin_id,
			path: pagePath.substring(1),
			type: debug ? 1 : 0,
			webUrl: h5Link,
		},
		success: function (res) {
			console.log("share:success: ", res);
		},
		fail: function (err) {
			console.log("share:err: ", err);
		},
	});
	// #endif
};

/**
 * 跳转到指定的网页 小程序需要配置业务域名白名单
vk.myfn.urlTo(path);
 */
myfn.urlTo = function (path) {
	uni.vk.navigateTo(`/pages/web-view/web-view?url=${encodeURIComponent(path)}`);
};

export default myfn;
