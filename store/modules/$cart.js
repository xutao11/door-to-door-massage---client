/**
 * vuex 购物车状态管理模块
 */
let lifeData = uni.getStorageSync('lifeData') || {};
let $cart = lifeData.$cart || {};
export default {
	// 通过添加 namespaced: true 的方式使其成为带命名空间的模块
	namespaced: true,
	/**
	 * vuex的基本数据，用来存储变量
	 */
	state: {
		/**
		 * 用户本地购物车的信息
		 * js调用示例
		 * vk.getVuex('$cart.list');
		 * 页面上直接使用示例
		 * {{ vk.getVuex('$cart.list') }}
		 * js更新示例
		 * vk.setVuex('$cart.list', list);
		 * 此处list是1个对象数组，内部对象属性如下
		 * @param {String} sku_id 主要参数 - skuID
		 * @param {Number} buy_num 主要参数 - 购买数量
		 * @param {String} sku_name 展示参数 - sku名称
		 * @param {String} image 展示参数 - sku图片
		 * @param {Number} price 展示参数 - sku价格
		 * @param {Number} stock 展示参数 - sku库存
		 * @param {String} goods_id 展示参数 - 商品ID
		 * @param {String} goods_name 展示参数 - 商品名称
		 * @param {Object} cart_rule 展示参数 - 购物车规则
		 * @param {Boolean} action_checked 辅助参数 - 购物车是否选中
		 */
		list: $cart.list || []
	},
	/**
	 * 从基本数据(state)派生的数据，相当于state的计算属性
	 */
	getters: {
		/**
		 * 获取购物车总商品数量
		 * js调用示例
		 * vk.vuex.getters('$cart/getTotal');
		 */
		getTotal: (state) => {
			let list = state.list;
			let total = 0;
			for (let i in list) {
				total += list[i].buy_num;
			}
			return total;
		},
		/**
		 * 获取购物车总商品数量(已勾选的)
		 * js调用示例
		 * vk.vuex.getters('$cart/getBuyNumTotal');
		 */
		getBuyNumTotal: (state) => {
			let list = state.list;
			let total = 0;
			list.map(item => {
				if (item.action_checked) {
					total += item.buy_num;
				}
			});
			return total;
		},
		/**
		 * 获取购物车商品总价
		 * js调用示例
		 * vk.vuex.getters('$cart/getBuyPriceTotal');
		 */
		getBuyPriceTotal: (state) => {
			let list = state.list;
			let moneyTotal = 0;
			list.map(item => {
				if (item.action_checked) {
					moneyTotal += item.buy_num * item.price;
				}
			});
			return (moneyTotal / 100).toFixed(2);
		},
	},
	/**
	 * 提交更新数据的方法，必须是同步的(如果需要异步使用action)。
	 * 每个 mutation 都有一个字符串的 事件类型 (type) 和 一个 回调函数 (handler)。
	 * 回调函数就是我们实际进行状态更改的地方，并且它会接受 state 作为第一个参数，提交载荷作为第二个参数。
	 */
	mutations: {

	},
	/**
	 * 和mutation的功能大致相同，不同之处在于 ==》
	 * 1. Action 提交的是 mutation，而不是直接变更状态。
	 * 2. Action 可以包含任意异步操作。
	 */
	actions: {
		setCart(context, list) {
			// 判断购物车内的商品是否有变动
			let oldCart = vk.getVuex('$cart.list');
			if (JSON.stringify(oldCart) === JSON.stringify(list)) {
				// console.log("-----无需更新-----")
				return false;
			}
			console.log("-----更新本地购物车-----")
			vk.setVuex('$cart.list', list);
			// 防抖函数，限制0.5秒内只有最后一次修改才去请求云端
			vk.pubfn.debounce(() => {
				console.log("-----更新云端购物车-----")
				vk.callFunction({
					url: 'client/order.setCart',
					data: {
						list
					},
					success: (data) => {
						
					}
				});
			}, 500, false, "setCart");
		}
	}
}
