import en from './modules/en.json'
import zhHans from './modules/zh-Hans.json'
import zhHant from './modules/zh-Hant.json'
export default {
	en,
	'zh-Hans': zhHans,
	'zh-Hant': zhHant
}
