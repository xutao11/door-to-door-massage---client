
`上门服务` 是基于 `uniapp` 和 `uniCloud` 和 `vk`开发的一套完整上门服务服务系统，目前已适配h5、微信小程序、APP（安卓和ios）

该系统基于以下 `uniCloud`和 和 `vk` 框架开发，有完善的开发文档，可扩展性强，便于你进行二次开发。（源码版可提供二次开发技术指导，作者QQ：2272723604）

注：本端仅为前端开源，如有需要后端，请联系本人qq 2272723604

## 用户端演示

[传送门-用户端演示](http://anmo.zhongwanyijia.com/user)

[传送门-用户端app下载地址](https://mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.cdn.bspapp.com/cloudstorage/25dda919-3e49-46e5-8a79-b29246e1d4e7.apk)


## 后台管理系统演示

[传送门-admin管理后台](http://anmo.zhongwanyijia.com/admin)


## 师傅端口系统演示

[传送门-师傅端](http://anmo.zhongwanyijia.com/shifu)

[传送门-师傅端app下载地址](https://mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.cdn.bspapp.com/cloudstorage/05b706db-a0cf-4099-b984-0be9f53647d0.apk)

admin（商家管理端）账号

* 帐号 test0001 密码 123456 角色：商城管理员（拥有大部分权限


client（客户端）账号
* 可以自己注册

shifu(师傅端) 账号
* 帐号 17687654321	 密码 123456 


> 项目作者QQ：2272723604（需要二开源码版本可联系）

> client端开发框架： [vk-unicloud-router](https://vkdoc.fsq.pub/client/)

> admin端开发框架： [vk-unicloud-admin](https://vkdoc.fsq.pub/admin/)

## 预览图片

___体验地址在后面___

![](https://mp-bce474d5-9cf7-459c-b834-31a92dd961b6.cdn.bspapp.com/cloudstorage/a26112e5-2cb9-4817-a66a-3857134a578f.png)

![](https://mp-bce474d5-9cf7-459c-b834-31a92dd961b6.cdn.bspapp.com/cloudstorage/73f29afd-2e3d-46cf-b8bc-f414d6a011a4.png)

**友情提示：**

* 1、虽然商城支持编译App，但如果你不懂App上架的流程，包括你不知道如何生成App证书，不知道怎么上架各大应用市场，特别是IOS还需要注册并购买开发者账号（1年要99美元），那么对于你来所，你想发布App就会很麻烦，作者没有义务帮你发布App

* 2、如果你只是想发布 `H5` 或 `微信小程序`，但你也不懂发布流程，基于 `H5` 和 `微信小程序` 的发布流程还是比较简单的，故作者可以指导你发布 `H5` 和 `微信小程序`

* 3、H5发布前提：你有你自己的已经备案的域名（作者不帮忙备案域名）

* 4、微信小程序发布前提：你有你已经注册并认证成功的微信小程序（作者不帮忙注册微信小程序）

* 5、App发布前提：你懂如何上架App，你知道怎么申请ios账号，知道如何申请微信开放平台等等。

* 6、使用第三方支付功能前提：你得有企业营业执照或个体工商户营业执照，你懂如何申请微信支付商户、支付宝支付商户、懂下载证书（只要证书和密钥都有，作者可以指导如何配置支付功能）

## 上门服务功能思维导图

![](https://mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.cdn.bspapp.com/cloudstorage/ebbcaa35-4197-4731-8151-ce637e198d7d.png)

* 更多功能请直接试用体验

## 后台管理系统演示

[传送门-admin管理后台](http://anmo.zhongwanyijia.com/admin)


## 师傅端口系统演示

[传送门-师傅端](http://anmo.zhongwanyijia.com/shifu)

admin（商家管理端）账号

* 帐号 test0001 密码 123456 角色：商城管理员（拥有大部分权限


client（客户端）账号
* 可以自己注册

shifu(师傅端) 账号
* 帐号 17612345678 密码 123456 


## 客户端演示

### H5演示

![](https://mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.cdn.bspapp.com/cloudstorage/c6a23bc6-175c-49d4-ba01-0f732ae8ea4f.png)

### APP演示

![](https://mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.cdn.bspapp.com/cloudstorage/aa4d77d7-541c-49fe-8e67-21f390189d65.png)

[安卓apk下载地址](https://mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.cdn.bspapp.com/cloudstorage/72cc837b-e679-49da-85e0-10e1072393f3.apk)

**注意：**

* 演示版的微信登录是关闭的（想体验app微信登录你需要自行编译，并用自定义基座，同时在admin后台平台管理-App-开启微信登陆）
* 演示版的微信支付是关闭的
* 演示版的本机号码一键登录是关闭的
* 无ios演示版，ios请自行编译安装
* 上面的安卓app下载地址用的是阿里云官方的 `vkceyugu.cdn.bspapp.com` 域名，该域名已被微信拉黑，可以用别的软件扫一扫安装或在电脑上下载后安装到手机。
* 如果安卓版也无法安装，则请自行编译安装
* 涉及到第三方支付时，APP必须打自定义基座。

## 简易安装部署教程

导入项目到HBX

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/1d18392c-60df-4f4f-8ded-60ec8a9eb7b7.png)

选择服务空间，目前建议选腾讯云，因为阿里云还未收费，不太稳定，同时阿里云的定时任务不行（最快也只有1小时执行1次）。当然你选择阿里云也可以，定时任务可以通过宝塔服务达到最快1分钟执行1次。（等阿里云收费后，反而建议选阿里云空间，因为阿里云空间会比腾讯云更实在）

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/293fed24-adf1-4ed5-91a9-0e5785191d65.png)

选择完服务空间，点击开始部署

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/4c221c76-b785-4781-8ee7-8f6073992045.png)

**如果出现是否需要替换，则点替换。**

如果部署失败，则可以手动部署（一般是hbx的问题）

* 1、右键 `common` 目录下每个模块，上传（注意：最好是一个一个上传，即一个上传成功了，再上传下一个，如果某个上传失败，就重试几次）

* 2、右键 `cloudfunctions` 目录下每个云函数（common除外，他是公共模块目录，不是云函数），上传（注意：最好是一个一个上传，即一个上传成功了，再上传下一个，就重试几次）

* 3、右键 `database/db_init.json` 文件，初始化数据库（如果初始化失败，就重试几次）

**当初始化数据库时，可能会超时，特别是阿里云空间，不用慌，其实空间还在正常部署，只是hbx等待时间过程报超时了，你可以等2分钟，然后再尝试右键初始化数据库，如果hbx提示没有任何表数据要上传，说明已经成功了。**

等待部署完成后，部署教程才正式开始。

### 1、修改uni-id配置

打开文件 `uniCloud/cloudfunctions/common/uni-config-center/uni-id/config.json`

此为uni-id的配置(主要用于配置微信登录等) [配置各参数介绍](https://vkdoc.fsq.pub/client/uniCloud/config/uni-id.html)

### 2、修改uni-pay配置

打开文件 `uniCloud/cloudfunctions/common/uni-config-center/uni-pay/config.js`

此为支付配置 [支付配置教程](https://vkdoc.fsq.pub/vk-uni-pay/config.html)

**修改完后记得右键上传 `uni-config-center` 公共模块**

### 3、修改定时器`z_timer_calc_reward`的定时时间

为了兼容阿里云，`z_timer_calc_reward` 的定时器默认是1小时执行1次，如果你选的是腾讯云，建议这里改成10分钟执行1次

`z_timer_calc_reward/package.json` 文件内的

```js
"cloudfunction-config": {
	"concurrency": 1,
	"memorySize": 128,
	"path": "",
	"timeout": 590,
	"triggers": [
		{
			"config": "0 0 * * * * *",
			"name": "z_timer_calc_reward",
			"type": "timer"
		}
	],
	"runtime": "Nodejs8"
},
```

改成

```js
"cloudfunction-config": {
	"concurrency": 1,
	"memorySize": 128,
	"path": "",
	"timeout": 590,
	"triggers": [
		{
			"config": "0 */10 * * * * *",
			"name": "z_timer_calc_reward",
			"type": "timer"
		}
	],
	"runtime": "Nodejs8"
},
```

也就是将 `config` 的配置该成10分钟运行一次。

___如果坚持使用阿里云，同时搭配宝塔来触发定时任务，则___

`z_timer_calc_reward/package.json` 文件内的

```js
"cloudfunction-config": {
	"concurrency": 1,
	"memorySize": 128,
	"path": "",
	"timeout": 590,
	"triggers": [
		{
			"config": "0 0 * * * * *",
			"name": "z_timer_calc_reward",
			"type": "timer"
		}
	],
	"runtime": "Nodejs8"
},
```

改成

```js
"cloudfunction-config": {
	"concurrency": 1,
	"memorySize": 128,
	"path": "/http/z_timer_calc_reward",
	"timeout": 60,
	"runtime": "Nodejs8"
},
```

也就是取消阿里云自己的定时任务，同时把该定时任务url化，再把url地址写到宝塔的计划任务里运行。

### 4、导入admin管理项目

### 5、admin管理项目右键 uniCloud 目录，绑定商城的client端

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/bca220cf-7824-4bbe-bb7d-8ab9fe2e33f5.png)

* 注意是绑定，不是关联
* 注意是绑定，不是关联
* 注意是绑定，不是关联

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/6163b7ad-ba36-43b2-9513-07900994e2ba.png)

### 6、分别启用client端和admin端项目和师傅锻。

### 7、进入admin端，输入admin账号，密码123456，点击菜单【用户角色权限】-【应用管理】- 将你 `AppID` 编辑成你的 `dcloud_appid`

dcloud_appid获取方法

项目根目录下的 `manifest.json`

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-cf0c5e69-620c-4f3c-84ab-f4619262939f/b7c6f637-6d6d-468c-904d-a7d4132400c6.png)

注意 `client端` 和 `admin端` 都要改成你自己的 `AppID`

**超级管理员账号：admin 密码：123456 进行登录**

## 问题

### 有bug，有些功能好像不正常？

别着急，来找作者反馈，只要确认是bug，一定会修复。

### 如何更改admin账号密码？

登录admin后台，右上角有更改密码按钮

### 小程序体验版或正式版无法请求云函数？

[传送门](https://vkdoc.fsq.pub/client/question/q10.html)

### 云函数请求报跨域错误？

[传送门](https://vkdoc.fsq.pub/client/question/q11.html)

### uni-config-center内的配置参数有说明吗？

[传送门](https://vkdoc.fsq.pub/client/uniCloud/config/uni-id.html)

### 第三方支付报错

一般都是配置错误或证书错误 [传送门](https://vkdoc.fsq.pub/vk-uni-pay/config.html#%E5%AE%8C%E6%95%B4%E7%9A%84%E6%94%AF%E4%BB%98%E9%85%8D%E7%BD%AE)

