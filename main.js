import App from "./App"
import store from "./store"
import config from "@/app.config.js"

// 引入 uView UI
import uView from "./uni_modules/vk-uview-ui"
// 引入 vk框架前端
import vk from "./uni_modules/vk-unicloud"
// 引入 国际化 配置
import messages from "./locale/index"
let locale = uni.getLocale()
// let localeObj = {
// 	"zh_CN": "zh-Hans", // 中国大陆（简体）
// 	"zh_HK": "zh-Hant", // 香港（繁体）
// 	"zh_MO": "zh-Hant", // 澳门（繁体）
// 	"zh_SG": "zh-Hans", // 新加坡（简体）
// 	"zh_TW": "zh-Hant", // 台湾（繁体）
// };
// if (localeObj[locale]) locale = localeObj[locale];
let i18nConfig = {
	locale,
	messages
}
//console.log('i18nConfig: ', i18nConfig)
// #ifndef VUE3
import Vue from "vue"
// 引入 国际化 配置
import VueI18n from "vue-i18n"
Vue.use(VueI18n)
const i18n = new VueI18n(i18nConfig)

// 引入 uView UI
Vue.use(uView)

// 引入 vk框架前端
Vue.use(vk)

// 初始化 vk框架
Vue.prototype.vk.init({
	Vue, // Vue实例
	config // 配置
})

Vue.config.productionTip = false

App.mpType = "app"

const app = new Vue({
	i18n,
	store,
	...App
})

app.$mount()
// #endif
