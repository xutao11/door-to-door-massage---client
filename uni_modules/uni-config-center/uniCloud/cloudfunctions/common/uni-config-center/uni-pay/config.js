const fs = require('fs');
const path = require('path')
module.exports = {
	// 统一 - 支付回调地址,格式为 "服务空间ID":"URL化地址"
	"notifyUrl": {
		// 本地开发环境
		"mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497": "https://fc-mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.next.bspapp.com/http/vk-pay",
		// 线上正式环境
		"mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497": "https://fc-mp-c1a03db8-e50e-4e5c-bf90-8d16b06a7497.next.bspapp.com/http/vk-pay"
	},
	// 此密钥主要用于跨云函数回调或回调java、php等外部系统时的通信密码（建议修改成自己的，最好64位以上，更加安全）
	// 详细介绍：https://vkdoc.fsq.pub/vk-uni-pay/uniCloud/pay-notify.html#%E7%89%B9%E5%88%AB%E6%B3%A8%E6%84%8F
	"notifyKey": "B4498B860EAB3CA3ADF9D07F891D7D78B4498B860EAB3CA3ADF9D07F891D7D78",
	// 是否将支付宝APP支付转H5支付 开启后无需再申请APP支付(共用当面付API)
	// "alipayAppPayToH5Pay":true,
	// 支付宝系统服务商ID，可不填
	"sysServiceProviderId": "2088731216435275",
	// 微信
	"wxpay": {
		// 微信 - 小程序支付
		"mp-weixin": {
			"appId": "",
			"secret": "",
			"mchId": "",
			"key": "",
			"pfx": fs.readFileSync(__dirname + '/wxpay/wxpay.p12')
		},
		// 微信 - APP支付
		"app-plus": {
			"appId": "",
			"secret": "",
			"mchId": "",
			"key": "",
			"pfx": fs.readFileSync(__dirname + '/wxpay/wxpay.p12')
		},
		// 微信 - H5网站二维码支付
		"h5": {
			"appId": "",
			"secret": "",
			"mchId": "",
			"key": "",
			"pfx": fs.readFileSync(__dirname + '/wxpay/wxpay.p12')
		},
		// 微信 - 公众号支付
		"h5-weixin": {
			"appId": "",
			"secret": "",
			"mchId": "",
			"key": "",
			"pfx": fs.readFileSync(__dirname + '/wxpay/wxpay.p12')
		},
		// 微信 - 手机外部浏览器H5支付
		"mweb": {
			"appId": "",
			"secret": "",
			"mchId": "",
			"key": "",
			"pfx": fs.readFileSync(__dirname + '/wxpay/wxpay.p12'),
			// 场景信息，必填
			"sceneInfo": {
				"h5_info": {
					"type": "Wap", // 此值固定Wap
					"wap_url": "https://www.xxxx.com", // 你的H5首页地址，必须和你发起支付的页面的域名一致。
					"wap_name": "vkmall", // 你的H5网站名称
				}
			}
		},
		// 微信 - 转账到零钱 v3版本
		"transfer": {
			"appId": "",
			"mchId": "",
			"apiV3key": "", // api v3密钥
			"appCertSn": "", // 商家应用证书的序列号
			"privateKey": "", // 商家私钥
			"wxpayPublicCertSn": "", // 微信支付公钥证书的序列号
			"wxpayPublicCertContent": "", // 微信支付公钥内容
		}
	},
	// 支付宝（证书记得选java版本）
	"alipay": {
		// 支付宝 - 小程序支付配置
		"mp-alipay": {
			"mchId": "",
			"appId": "",
			"privateKey": "",
			"alipayPublicCertPath": path.join(__dirname, 'alipay/alipayCertPublicKey_RSA2.crt'),
			"alipayRootCertPath": path.join(__dirname, 'alipay/alipayRootCert.crt'),
			"appCertPath": path.join(__dirname, 'alipay/appCertPublicKey.crt'),
			"sandbox": false
		},
		// 支付宝 - APP支付配置
		"app-plus": {
			"mchId": "",
			"appId": "",
			"privateKey": "",
			"alipayPublicCertPath": path.join(__dirname, 'alipay/alipayCertPublicKey_RSA2.crt'),
			"alipayRootCertPath": path.join(__dirname, 'alipay/alipayRootCert.crt'),
			"appCertPath": path.join(__dirname, 'alipay/appCertPublicKey.crt'),
			"sandbox": false
		},
		// 支付宝 - H5支付配置（包含：网站二维码、手机H5，需申请支付宝当面付接口权限）
		"h5": {
			"mchId": "2088241237416981",
			"appId": "2021003100665797",
			"privateKey": "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCKXVmab9tQjgGersTqQCJyet5L+vlY5XlxQ1EyGoebcCjANTJKrAWyrXOcjy2tG93Tb8IKsrDzo01BJuQ7tALiO9IVMFcXP52YCrRAmiOrWw31C3RIZ40WXLKroZXst6vwHuU45ByWWmFshDzt10/BW3MWcDI5p4hTmS+Ii/TgjEZffKI59pHsIU5S63aj/LQS7wVu2Urdg7OdJdFgPg77rFB8dcU1FdR3IWv5UL9tPn+AypGyb7XlZ1/ZNSKgwnCSDdsr11n3a84FnXAel8fwkc47CwYJfTqKKKta5xQepkQTsE0ZcTpQyUrfnhTgFj7L75Ljb5H2k7j4NgmbEPmhAgMBAAECggEARF9mxMMbjA3wTPquAdeZbhq7OK6Xgs86mLacK/ciJbRh3sEqze19j1LY+3dhKdbIYhAqweCVQn/i0Bwo0jvc5PCMmr3de3aLKLO5aOcCU03Wcs2rM5BADtnVzKw3itckVq7dtKQ8YCvfUqFx/lHUbocDMwZUO1tHKZBcBcyrQD42yTtaI8NIyvQTjwoRcYCGjhYOkih1gz0uHPNgNhkr7MxaSss2h6WXNRcSwrisyE5OADXzB5v//7lMc5Ehe4PHoNsQiZ2QnRC1SrvkWEfQjnjCQFaFcfK3UolHj1SvKbctHzIijdnUQY7RZGlahHCb3PBr/5vSq1TP4nkkLH3MhQKBgQDG9YDrPb36+/dgatppsnmK/jG0wBqrspF4Qh41IpFMKqxeO5s4jyzbagClxmuxnIuztl6G9962fa0eN4YU6ps3VTQKYCTHCWrVuWiLSlOqBWL5SR5/0rfO+UdIV6thVknWMQRXR6F+Ry/7nbubATp8dHtYwUnvZKtRGqsonNJ4lwKBgQCyCI3psbjTiQGF+t4NYa7buAiy7kq0/Nbi0+A6Ev5Is6uhyctKV8r9oXgTuUZFs/akLuC5YgwSc0Qv/y+qcdZIxArzBsMZ7KxYXsohNpoJVQLb7J0W9vQk1LkuhPFTQbro6g2IRg7Mhifb5DrAJY0IK0uGeIBhiwbaihklY6HuhwKBgHIlqDFO2xCEPwkyRN0faFw2Oo5ZKPQZHxy/0lTp4DKyyPHHgOaSPkokJvr1PBRKnFwe5VzKGRzaPdh4QpT9hM/aMmFBh9gprwGvvissGZQSQxaCvNzZKFu26Q64Pbs6pmIc/UCV+DFa5kyV0my757I/ogTp4GFYgHGnu0sxrg9BAoGAKTcZw3RFWZNqKt488dQfavZb7TCFUwkFplVun4MuIx6VKUtSKM/SSo1LAyXtQs2615+Sntg3+z18F7B4FHluJkJcVo7GNAY6j+rB2JaPaoGnu8o3Q83AD5a/mClmxON+pdUpdNbjLi4QpDNd9oW69J0xStwyvGI3H/jAgYtjPZkCgYEAwXzbrJL8E5EuCcIwcKBvZc6tSNy8RttYD3Bus68bMdsePNPom5KsdkRBlIG7Fmy/T1ACI45eJmrBC0pRjit/jeXbfmrXWMaiLH0ALY08b9KHWsElvNc6w1nmaEH7XHcmUPCZPUH4s0TLCrv7FGyIpALvDR2r/AtgqHOJqNZju4I=",
			"alipayPublicCertPath": path.join(__dirname, 'alipay/alipayCertPublicKey_RSA2.crt'),
			"alipayRootCertPath": path.join(__dirname, 'alipay/alipayRootCert.crt'),
			"appCertPath": path.join(__dirname, 'alipay/appCertPublicKey.crt'),
			"sandbox": false
		},
		// 支付宝 - 转账到支付宝等资金转出接口，其中 appCertSn 和 alipayRootCertSn 通过工具获取
		"transfer": {
			"mchId": "",
			"appId": "",
			"privateKey": "",
			"appCertSn": "",
			"alipayRootCertSn": "",
			"sandbox": false
		}
	}
}
