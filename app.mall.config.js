export default {
	maill: {
		order: {
			// vk.getVuex('$app.config.maill.order.payType');
			payType: {
				"wxpay": "微信支付",
				"alipay": "支付宝支付",
				"balance": "余额支付",
				"offline": "线下支付",
				"none": "无需支付"
			},
			// vk.getVuex('$app.config.maill.order.status');
			status: {
				"-4": "已关闭",
				"-3": "已全额退款",
				"-2": "申请退款中",
				"-1": "已取消",
				"0": "未付款",
				"1": "已付款",
				"2": "准备发货中",
				"3": "已部分发货",
				"4": "已全部发货",
				"5": "已收货",
				"6": "已完成"
			},
			// vk.getVuex('$app.config.maill.order.status2');
			status2: {
				"-4": "订单已关闭",
				"-3": "订单已全额退款",
				"-2": "等待卖家审核",
				"-1": "订单已取消",
				"0": "等待买家付款",
				"1": "等待商家发货",
				"2": "等待商家发货",
				"3": "等待买家收货",
				"4": "等待买家收货",
				"5": "等待买家评价",
				"6": "订单已完成"
			}
		}
	}
}
